import React from "react";
import "./style.css";

class App extends React.Component {
  constructor() {
    super();
    this.state = {
      inputName: "",
      isNewBlockVisible: false,
      isButtonAddListVisible: true,
      columnListVisible: true,
      cards: []
    };
    this.createList = this.createList.bind(this);
    this.addNewList = this.addNewList.bind(this);
    this.cancelList = this.cancelList.bind(this);
    this.removeCard = this.removeCard.bind(this);
  }
  addNewList() {
    this.setState({
      isNewBlockVisible: true,
      isButtonAddListVisible: false,
      columnListVisible: false
    });
  }

  cancelList() {
    this.setState({
      isNewBlockVisible: false,
      isButtonAddListVisible: true
    });
  }
  createList() {
    this.setState({
      isNewBlockVisible: false
    });
    let newCards = this.state.cards;
    if (this.state.inputName) {
      newCards.push({
        name: this.state.inputName
      });

      this.setState({
        ...this.state,
        cards: newCards
      });
    }
  }

  changeName(e) {
    this.setState({
      ...this.state,
      inputName: e.target.value
    });
  }

  printCards() {
    return this.state.cards.map((card, index) => (
      <div className="List">
        <div key={index} className="listName">
          {card.name}
        </div>
        <div className="buttonAddCard">+ Добавить карточку</div>
        <div
          className="buttonDeleteList"
          onClick={() => this.removeCard(index)}
        >
          X
        </div>
      </div>
    ));
  }

  removeCard(index) {
    let newCards = this.state.cards;
    newCards.splice(index, 1);

    this.setState({
      ...this.state,
      cards: newCards
    });
  }
  render() {
    const cards = this.printCards();

    return (
      <div>
        <div className="top">
          <div className="logo"></div>
        </div>
        <div className="center">
          <div
            className={
              this.state.isButtonAddListVisible
                ? "buttonAddList"
                : "buttonAddList buttonAddListVisible"
            }
            onClick={this.addNewList}
          >
            + Добавить список
          </div>
          <div
            className={
              this.state.columnListVisible
                ? "columnList"
                : "columnList columnListVisible"
            }
          >
            <div
              className={
                this.state.isNewBlockVisible
                  ? "newBlock newBlockVisible"
                  : "newBlock"
              }
            >
              <div className="inputNameList">
                <input
                  placeholder="Введите заголовок списка"
                  value={this.state.inputName}
                  onChange={this.changeName.bind(this)}
                ></input>
              </div>
              <div className="buttonAcceptList" onClick={this.createList}>
                Добавить список
              </div>
              <div className="buttonCancelList" onClick={this.cancelList}>
                Х
              </div>
            </div>
            {cards}
          </div>
        </div>
      </div>
    );
  }
}

export default App;
